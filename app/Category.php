<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'order'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function organizer()
    {
        return $this->hasOne(Organizer::class);
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }
}
