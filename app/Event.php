<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use ZigaStrgar\Orderable\Orderable;

class Event extends Model
{
    use Orderable, SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'price',
        'organizer_id',
        'venue_id',
        'published',
        'facebook_id',
        'link',
        'begins_at',
        'ends_at',
        'hash'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'begins_at',
        'ends_at',
    ];

    public function organizer()
    {
        return $this->belongsTo(Organizer::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function venue()
    {
        return $this->belongsTo(Venue::class);
    }

    public function scopeInFuture($query)
    {
        return $query->where('begins_at', '>=', Carbon::now());
    }

    public function scopeUnpublished($query)
    {
        return $query->where('published', false);
    }

    public function getCategoryListAttribute()
    {
        return $this->categories->pluck('id')->toArray();
    }

    public function orderable()
    {
        return [
            'id' => 'DESC',
        ];
    }
}
