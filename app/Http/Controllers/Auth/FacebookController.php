<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk as FB;
use Facebook\Exceptions\FacebookSDKException as FBException;

class FacebookController extends Controller
{
    public function login(FB $fb)
    {
        $login_url = $fb->getLoginUrl([ 'email' ]);

        return view('auth.login', compact('login_url'));
    }

    public function callback(FB $fb)
    {
        // Obtain an access token.
        try {
            $token = $fb->getAccessTokenFromRedirect();
        } catch ( FBException $e ) {
            dd($e->getMessage());
        }

        if ( !$token ) {
            // Get the redirect helper
            $helper = $fb->getRedirectLoginHelper();

            if ( !$helper->getError() ) {
                abort(403, 'Unauthorized action.');
            }

            // User denied the request
            dd($helper->getError(), $helper->getErrorCode(), $helper->getErrorReason(), $helper->getErrorDescription());
        }

        if ( !$token->isLongLived() ) {
            // OAuth 2.0 client handler
            $oauth_client = $fb->getOAuth2Client();

            // Extend the access token.
            try {
                $token = $oauth_client->getLongLivedAccessToken($token);
            } catch ( FBException $e ) {
                dd($e->getMessage());
            }
        }

        $fb->setDefaultAccessToken($token);

        // Save for later
        Session::put('fb_user_access_token', (string)$token);

        // Get basic info on the user from Facebook.
        try {
            $response = $fb->get('/me?fields=id,name,email');
        } catch ( FBException $e ) {
            dd($e->getMessage());
        }

        // Convert the response to a `Facebook/GraphNodes/GraphUser` collection
        $facebook_user = $response->getGraphUser();

        // Create the user if it does not exist or update the existing entry.
        // This will only work if you've added the SyncableGraphNodeTrait to your User model.
        $user = User::createOrUpdateGraphNode($facebook_user);

        // Log the user into Laravel
        Auth::login($user);

        return redirect('/');
    }
}