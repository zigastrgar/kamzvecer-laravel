<?php

namespace App\Http\Controllers;

use App\Category;
use App\Event;
use App\Http\Requests\EventRequest;
use App\Organizer;
use App\Venue;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Http\Requests;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events     = Event::inFuture()->unpublished()->get();
        $organizers = Organizer::all();
        $venues     = Venue::all();
        $categories = Category::all();

        return view('events.index', compact('events', 'organizers', 'venues', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $venues     = Venue::pluck('name', 'id');
        $organizers = Organizer::pluck('name', 'id');
        $categories = Category::pluck('name', 'id');

        return view('events.create', compact('venues', 'organizers', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\EventRequest|\Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(EventRequest $request)
    {
        $event = Event::create($request->all());

        $this->categories($request, $event);

        return redirect('events');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::findOrFail($id);

        return view('events.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event      = Event::findOrFail($id);
        $venues     = Venue::pluck('name', 'id');
        $organizers = Organizer::pluck('name', 'id');
        $categories = Category::pluck('name', 'id');

        return view('events.edit', compact('event', 'venues', 'organizers', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\EventRequest|\Illuminate\Http\Request $request
     * @param  int                                                     $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(EventRequest $request, $id)
    {
        $event = Event::findOrFail($id);
        $price = ( strlen($request->input('price')) > 0 ) ? $request->input('price') : null;
        $event->update(array_merge($request->all(), [ 'price' => $price ]));
        $this->categories($request, $event);

        return redirect('events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Event::findOrFail($id)->delete();

        return "success";
    }

    /**
     * @param $id
     *
     * @return string
     */
    public function publish($id)
    {

        $client     = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'http://kamzvecer.si',
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]);
        $event      = Event::findOrFail($id)->fresh();
        $categories = $event->categories->toArray();

        $data = [
            'dogodek' => $event->name,
            'opis'    => $event->description,
            'cena'    => (int)$event->price,
            'datum'   => $event->begins_at->format('Y-m-d'),
            //'datum_konec' => $event->ends_at->format('Y-m-d'),
            'ura'     => ( !is_null($event->ends_at) ) ? $event->begins_at->format('H:i') : null,
            //'ura_konec'   => ( !is_null($event->ends_at) ) ? $event->ends_at->format('H:i') : null,
            'lokal'   => ( !is_null($event->venue->code_register) ) ? $event->venue->code_register : $event->organizer->id,
            'zvrst1'  => ( isset($categories[0]['id']) ) ? "zvrst" . $categories[0]['id'] : null,
            'zvrst2'  => ( isset($categories[1]['id']) ) ? "zvrst" . $categories[1]['id'] : null,
            'zvrst3'  => ( isset($categories[2]['id']) ) ? "zvrst" . $categories[2]['id'] : null,
            'link'    => $event->link,
            'regija'  => 'regija1',
            'sponzor' => 1
        ];

        $response = $client->post('/cms/includes/dogodek_create.php', [
            'form_params' => $data
        ]);

        if ( $response->getStatusCode() == 200 ) {
            Event::findOrFail($id)->update([ 'published' => true ]);

            return "success";
        }
    }

    public function update_venue(Event $event, Request $request)
    {
        $event->venue_id = $request->input('venue');
        $event->save();

        return "success";
    }

    public function update_organizer(Event $event, Request $request)
    {
        $event->organizer_id = $request->input('organizer');
        $event->save();

        return "success";
    }

    public function update_description(Event $event, Request $request)
    {
        $event->name        = $request->input('title');
        $event->price       = ( strlen($request->input('price') > 0) ) ? $request->input('price') : null;
        $event->description = $request->input('content');
        $this->categories($request, $event);
        $event->save();

        return "success";
    }

    /**
     * @param \App\Http\Requests\EventRequest|\Illuminate\Http\Request $request
     * @param \App\Event                                               $event
     */
    private function categories(Request $request, Event $event)
    {
        $event->categories()->sync($request->input('category_list'));
    }
}
