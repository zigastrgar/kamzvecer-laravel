<?php

namespace App\Http\Controllers;

use App\Event;
use App\Venue;
use Illuminate\Http\Request;

use App\Http\Requests;

class ExportsController extends Controller
{
    public function events()
    {
        return Event::with([ 'venue', 'organizer', 'categories' ])->inFuture()->where('published', true)->get();
    }

    public function event($id)
    {
        return Event::with([ 'venue', 'organizer', 'categories' ])->where('id', $id)->inFuture()
            ->where('published', true)->first();
    }

    public function usernames()
    {
        return Venue::pluck('code_register', 'name');
    }
}
