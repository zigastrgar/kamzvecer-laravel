<?php

namespace App\Http\Controllers;

use App\Event;
use App\Organizer;
use App\Venue;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use League\Flysystem\Exception;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Exceptions\FacebookResponseException;

class FacebookController extends Controller
{
    protected $fb;

    public function __construct(LaravelFacebookSdk $fb)
    {
        $this->fb = $fb;
    }

    public function fetchEvents(Request $request)
    {
        $offset     = ( $request->has('page') ) ? $request->input('page') : 1;
        $organizers = Organizer::paginate(50);
        foreach ( $organizers as $organizer ) {
            if ( !empty( $organizer->facebook_id ) ) {
                $events = $this->getEvents($organizer);
                foreach ( $events as $event ) {
                    $start_time = Carbon::parse($event['start_time']);
                    if ( $start_time->isFuture() ) {
                        $name  = isset( $event['place']['name'] ) ? $event['place']['name'] : $organizer->name;
                        $venue = Venue::firstOrCreate([ 'name' => $name ]);

                        $eventData = [
                            'name'         => htmlspecialchars($event['name']),
                            'description'  => ( isset( $event['description'] ) ) ? htmlspecialchars($event['description']) : "",
                            'price'        => $this->getPriceFromDescription(( isset( $event['description'] ) ) ? $event['description'] : ""),
                            'begins_at'    => $start_time,
                            'ends_at'      => ( isset( $event['end_time'] ) ) ? Carbon::parse($event['end_time']) : null,
                            'link'         => "https://www.facebook.com/events/{$event["id"]}/",
                            'facebook_id'  => $event['id'],
                            'organizer_id' => $organizer->id
                        ];

                        try {
                            $eventActive = Event::withTrashed()->where('facebook_id', $event['id'])->first();

                            if ( !is_null($eventActive) ) {
                                $eventActive->update($eventData);
                                $eventActive->fresh();
                            } else {
                                $eventActive = Event::create($eventData);
                            }

                            if ( !is_null($venue) ) {
                                $eventActive->venue()->associate($venue);
                                $eventActive->save();
                            }

                            $eventActive->categories()->sync([ $organizer->category_id ]);
                        } catch ( Exception $e ) {

                        }
                    }
                }
            }
        }

        if ( $organizers->lastPage() > $offset ) {
            $offset++;

            return redirect()->away(url('fetch_events?page=' . $offset));
        }

        return redirect('events');
    }

    private function getEvents($organizer)
    {
        try {
            $response =
                $this->fb->get('/' . $organizer->facebook_id . "/events?fields=id,name,description,cover,place,start_time,end_time", Session::get('fb_user_access_token'));
        } catch ( FacebookResponseException $e ) {
            echo 'Graph returned an error: ' . $e->getMessage();
            if ( $e->getCode() == 100 ) {
                $organizer->delete();
            }

            return;
        } catch ( FacebookSDKException $e ) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();

            return;
        }

        return $response->getDecodedBody()['data'];
    }

    private function getPriceFromDescription($description)
    {
        foreach ( explode(PHP_EOL, mb_strtolower($description)) as $line ) {
            if ( str_contains($line, [
                    "ni vstopnine",
                    "brez vstopnine",
                    "vstopnine ni",
                    "vstop brezplačen",
                    "prost vstop",
                    "je brezplačen",
                    "je free"
                ]) !== false
            ) {
                return 0;
            }

            if ( preg_match("/(<?[0-9]+) ?(eu|vr|e|€)/", $line, $results) == 1 ) {
                return (int)$results[1];
            }
        }

        return null;
    }
}
