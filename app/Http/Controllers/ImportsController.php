<?php

namespace App\Http\Controllers;

use App\Category;
use App\Event;
use App\Http\Requests\EventRequest;
use App\Organizer;
use App\Venue;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use Facebook\Exceptions\FacebookSDKException;


class ImportsController extends Controller
{
    protected $fb;

    public function __construct(LaravelFacebookSdk $fb)
    {
        $this->fb = $fb;
    }

    public function locals()
    {
        $locals = DB::table('lokali')->where('id', '>', '270')->get();

        foreach ( $locals as $local ) {
            $local = (array)$local;
            if ( !empty($local['name']) || !empty($local['link']) ) {
                $category =
                    Category::whereRaw("lower(name) = '" . mb_strtolower($local['category'] . "'", "UTF-8"))->first();
                $local    = $this->checkLink($local);
                Organizer::create(array_merge($local, [ 'category_id' => ( $category ) ? $category->id : 12 ]));
            }
        }
    }

    private function checkLink($data)
    {
        if ( strpos($data['link'], "facebook.com/") !== false ) {
            $facebookInfo = $this->getUserInformation($this->getFacebookUsername($data['link']));
            if ( isset($facebookInfo['location']) ) {
                $address = ( isset($facebookInfo['location']['street']) ) ? $facebookInfo['location']['street'] : "";
                $city    = ( isset($facebookInfo['location']['zip']) ) ? $facebookInfo['location']['zip'] . " " : "";
                $city .= ( isset($facebookInfo['location']['city']) ) ? $facebookInfo['location']['city'] : "";
                if ( !isset($facebookInfo['location']['latitude']) && !isset($facebookInfo['location']['longitude']) ) {
                    $result = $this->addressToGeoLocation($address . "," . $city);
                    $lat    = $result['lat'];
                    $lng    = $result['lng'];
                }
                Venue::firstOrCreate([
                    'name'      => $facebookInfo['name'],
                    'address'   => $address,
                    'city'      => $city,
                    'latitude'  => ( isset($facebookInfo['location']['latitude']) ) ? $facebookInfo['location']['latitude'] : $lat,
                    'longitude' => ( isset($facebookInfo['location']['longitude']) ) ? $facebookInfo['location']['longitude'] : $lng,
                ]);
            }

            $data = array_merge([
                'facebook_id' => $facebookInfo['id'],
                'link'        => $facebookInfo['link'],
                'name'        => $facebookInfo['name'],
                'about'       => ( isset($facebookInfo['about']) ) ? $facebookInfo['about'] : "",
                'address'     => ( isset($address) && isset($city) ) ? $address . ", " . $city : null
            ], $data);

            $data['name'] = ( strlen($data['name']) > 0 ) ? $data['name'] : $facebookInfo['name'];

            $data['link'] = $facebookInfo['link'];
        }

        return $data;
    }

    private function getUserInformation($username)
    {
        try {
            $response =
                $this->fb->get('/' . $username . '?fields=id,name,about,location,link', Session::get('fb_user_access_token'));
        } catch ( FacebookSDKException $e ) {
            if ( $e->getCode() == 803 ) {
                $username = explode("?", $username)[0];
                $response = $this->fb->get('search?type=user&q=' . $username, Session::get('fb_user_access_token'));
                $data     = $response->getDecodedBody();
                $userid   = $data['data'][0]['id'];
                $response =
                    $this->fb->get('/' . $userid . '?fields=id,name,about,location,link', Session::get('fb_user_access_token'));
            } else {
                dump($username);
                dd($e->getMessage());
            }
        }

        return $response->getGraphUser();
    }

    private function getFacebookUsername($link)
    {
        if ( is_numeric($link) ) {
            return $link;
        }

        $www = "";
        if ( strpos($link, "//www.") !== false ) {
            $www = "www.";
        }

        $remaining = str_replace("https://{$www}facebook.com/", "", $link);
        $remaining = str_replace("?fref=ts", "", $remaining);
        $remaining = trim($remaining, "/");

        if ( str_contains($remaining, "pages/") ) {
            $ar = explode("/", $remaining);

            return end($ar);
        } else if ( preg_match('/\-(\d{5,})/', $remaining, $matches) ) {
            return $matches[1];
        }

        return explode("/", $remaining)[0];
    }

    private function addressToGeoLocation($address)
    {
        $geo =
            json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($address)), true);

        return [
            'lat' => $geo['results'][0]['geometry']['location']['lat'],
            'lng' => $geo['results'][0]['geometry']['location']['lng']
        ];
    }

    public function venues_generate()
    {
        foreach ( Organizer::where('id', '>', 165)->get() as $organizer ) {
            if ( !empty($organizer->facebook_id) ) {
                $facebookInfo = $this->getUserInformation($this->getFacebookUsername($organizer->facebook_id));
                if ( isset($facebookInfo['location']) ) {
                    $address =
                        ( isset($facebookInfo['location']['street']) ) ? $facebookInfo['location']['street'] : "";
                    $city    =
                        ( isset($facebookInfo['location']['zip']) ) ? $facebookInfo['location']['zip'] . " " : "";
                    $city .= ( isset($facebookInfo['location']['city']) ) ? $facebookInfo['location']['city'] : "";
                    if ( !isset($facebookInfo['location']['latitude']) && !isset($facebookInfo['location']['longitude']) ) {
                        $result = $this->addressToGeoLocation($address . "," . $city);
                        $lat    = $result['lat'];
                        $lng    = $result['lng'];
                    }
                    Venue::firstOrCreate([
                        'name'      => $facebookInfo['name'],
                        'address'   => $address,
                        'city'      => $city,
                        'latitude'  => ( isset($facebookInfo['location']['latitude']) ) ? round($facebookInfo['location']['latitude'], 5) : round($lat, 5),
                        'longitude' => ( isset($facebookInfo['location']['longitude']) ) ? round($facebookInfo['location']['longitude'], 5) : round($lng, 5),
                    ]);
                }
            }
        }
    }

    public function event(Request $request)
    {
        //dd($request->all());
        $price =
            ( strlen($request->input('price')) > 0 ) ? $request->input('price') : $this->getPriceFromDescription($request->input('description'));
        $event = Event::create(array_merge($request->all(), [ 'price' => $price, 'imported' => 1 ]));

        $event->categories()->sync($request->input('category_list'));

        return response("Uspešno dodan", 200);
    }

    private function getPriceFromDescription($description)
    {
        foreach ( explode(PHP_EOL, mb_strtolower($description)) as $line ) {
            if ( str_contains($line, [
                    "ni vstopnine",
                    "brez vstopnine",
                    "vstopnine ni",
                    "vstop brezplačen",
                    "prost vstop",
                    "je brezplačen",
                    "je free"
                ]) !== false
            ) {
                return 0;
            }

            if ( preg_match("/(<?[0-9]+) ?(eu|vr|e|€)/", $line, $results) == 1 ) {
                return (int)$results[1];
            }
        }

        return null;
    }
}
