<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\OrganizerRequest;
use App\Organizer;
use App\Venue;
use Illuminate\Support\Facades\Session;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use Illuminate\Http\Request;

use App\Http\Requests;

class OrganizersController extends Controller
{
    protected $fb;

    public function __construct(LaravelFacebookSdk $fb)
    {
        $this->fb = $fb;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organizers = Organizer::all();

        return view('organizers.index', compact('organizers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id');

        return view('organizers.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\OrganizerRequest|\Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(OrganizerRequest $request)
    {
        $data = $this->checkLink($request);

        Organizer::create($data);

        return redirect('organizers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $organizer = Organizer::findOrFail($id);

        return view('organizers.show', compact('organizer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $organizer  = Organizer::findOrFail($id);
        $categories = Category::pluck('name', 'id');

        return view('organizers.edit', compact('organizer', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\OrganizerRequest|\Illuminate\Http\Request $request
     * @param  int                                                         $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(OrganizerRequest $request, $id)
    {
        $data = $this->checkLink($request);
        Organizer::findOrFail($id)->update($data);

        return redirect('organizers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Organizer::findOrFail($id)->delete();

        return "success";
    }

    private function checkLink(OrganizerRequest $request)
    {
        $data = $request->all();
        if ( strpos($request->input('link'), "facebook.com/") !== false ) {
            $facebookInfo = $this->getUserInformation($this->getFacebookUsername($request->input('link')));
            Venue::firstOrCreate([
                'name'      => $facebookInfo['name'],
                'address'   => ( strlen($facebookInfo['location']['street']) > 0 ) ? $facebookInfo['location']['street'] : null,
                'city'      => $facebookInfo["location"]['zip'] . " " . $facebookInfo["location"]['city'],
                'latitude'  => ( strlen($facebookInfo['location']['latitude']) > 0 ) ? $facebookInfo['location']['latitude'] : null,
                'longitude' => ( strlen($facebookInfo['location']['longitude']) > 0 ) ? $facebookInfo['location']['longitude'] : null
            ]);

            $data = array_merge($data, [ 'facebook_id' => $facebookInfo['id'], 'link' => $facebookInfo['link'] ]);
        }

        return $data;
    }

    private function getUserInformation($username)
    {
        try {
            $response =
                $this->fb->get('/' . $username . '?fields=id,name,about,location,link', Session::get('fb_user_access_token'));
        } catch ( FacebookSDKException $e ) {
            dd($e->getMessage());
        }

        return $response->getGraphUser();
    }

    private function getFacebookUsername($link)
    {
        if ( is_numeric($link) ) {
            return $link;
        }

        $www = "";
        if ( strpos($link, "//www.") !== false ) {
            $www = "www.";
        }

        $remaining = str_replace("https://{$www}facebook.com/", "", $link);
        $remaining = str_replace("?fref=ts", "", $remaining);
        $remaining = trim($remaining, "/");

        if ( str_contains($remaining, "pages/") ) {
            $ar = explode("/", $remaining);

            return end($ar);
        } else if ( preg_match('/\-(\d{5,})/', $remaining, $matches) ) {
            return $matches[1];
        }

        return explode("/", $remaining)[0];
    }
}
