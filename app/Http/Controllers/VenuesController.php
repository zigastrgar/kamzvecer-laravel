<?php

namespace App\Http\Controllers;

use App\Http\Requests\VenueRequest;
use App\Venue;
use Illuminate\Http\Request;

use App\Http\Requests;
use League\Flysystem\Exception;

class VenuesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $venues = Venue::all();

        return view('venues.index', compact('venues'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('venues.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\VenueRequest|\Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(VenueRequest $request)
    {
        Venue::create(array_merge($request->all(), $this->addressToGeoLocation($request)));

        return redirect('venues');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $venue = Venue::findOrFail($id);

        return view('venues.show', compact('venue'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $venue = Venue::findOrFail($id);

        return view('venues.edit', compact('venue'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\VenueRequest|\Illuminate\Http\Request $request
     * @param  int                                                     $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(VenueRequest $request, $id)
    {
        $data = $request->all();

        if ( !$request->has('approved') ) {
            $data['approved'] = false;
        }
        Venue::findOrFail($id)->update(array_merge($data, $this->addressToGeoLocation($request)));

        return redirect('venues');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Venue::findOrFail($id)->delete();

        return "success";
    }

    public function approve($id)
    {
        Venue::findOrFail($id)->update([ 'approved' => true ]);

        return "success";
    }

    private function addressToGeoLocation(Request $request)
    {
        try {
            $address = $request->input('address') . ", " . $request->input('city');
            $geo     =
                json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($address)), true);

            return [
                'latitude'  => $geo['results'][0]['geometry']['location']['lat'],
                'longitude' => $geo['results'][0]['geometry']['location']['lng']
            ];
        } catch ( \Exception $exception ) {
            return [];
        }
    }
}
