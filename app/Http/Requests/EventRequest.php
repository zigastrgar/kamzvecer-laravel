<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'            => 'required|string',
            'description'     => 'string',
            'organizer_id'    => 'exists:organizers,id',
            'venue_id'        => 'exists:venues,id',
            'category_list.*' => 'exists:categories,id',
            'published'       => 'boolean',
            'begins_at'       => 'required|date',
            'ends_at'         => 'date',
        ];
    }
}
