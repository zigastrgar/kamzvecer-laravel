<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Venue extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'address',
        'city',
        'latitude',
        'longitude',
        'approved'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function events()
    {
        return $this->hasMany(Event::class);
    }

    public function getGoogleMapsAttribute()
    {
        return "https://www.google.si/maps/@" . $this->latitude . "," . $this->longitude . ",19z";
    }
}
