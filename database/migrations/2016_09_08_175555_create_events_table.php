<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->float('price')->nullable();
            $table->integer('organizer_id')->unsigned();
            $table->integer('venue_id')->unsigned()->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('link')->nullable();
            $table->boolean('published')->default(false);
            $table->boolean('imported')->default(false);
            $table->string('hash')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->timestamp('begins_at')->nullable();
            $table->timestamp('ends_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
