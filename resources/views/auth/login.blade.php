@extends('app')

@section('content')
    <h3 class="page-header text-center">Hey mate to see any of the cool features presented on this page please login with Facebook :)</h3>
    <a href="{{ $login_url }}" class="btn btn-primary">Login with facebook</a>
@endsection