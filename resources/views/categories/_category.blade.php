<tr data-category="{{ $category->id }}">
    <td>{{ $category->name }}</td>
    <td>
        <button class="btn btn-primary" onclick="editCategory({{ $category->id }})">Uredi</button>
        <button class="btn btn-danger" onclick="deleteCategory({{ $category->id }})">Izbriši</button>
    </td>
</tr>

@section('scripts')
    <script>
        function editCategory(id) {
            $.ajax({
                url: '/categories/' + id,
                type: 'GET',
                data: {'_token': '{{ csrf_token() }}'},
                success: function (response) {
                    $("#name2").val(response.name);
                    $("#id").val(response.id);
                    $("#edit-category").modal();
                }
            });
        }

        function deleteCategory(id) {
            $.ajax({
                url: '/categories/' + id,
                type: 'DELETE',
                data: {'_token': '{{ csrf_token() }}'},
                success: function (response) {
                    if (response === "success") {
                        $("tr[data-category=" + id + "]").remove();
                    }
                }
            })
        }

        function saveCategory() {
            var id = $("#id").val();
            var name = $("#name2").val();

            $.ajax({
                type: 'PATCH',
                url: '/categories/' + id,
                data: {'name': name, '_token': '{{ csrf_token() }}'},
                success: function (response) {
                    if (response === "success") {
                        $("tr[data-category=" + id + "]>td:first").text(name);
                    }
                    $("#edit-category").modal('hide');
                }
            });
        }
    </script>
@endsection