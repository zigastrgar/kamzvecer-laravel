<div class='form-group'>
    {!! Form::label('name', 'Ime kategorije') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::submit($submitText, ['class' => "btn btn-$color"]) !!}
</div>