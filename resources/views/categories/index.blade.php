@extends('app')

@section('content')
    <h1 class="page-header">Kategorije</h1>
    <h2 class="page-header">Dodajanje kategorije</h2>
    {!! Form::open(['url' => 'categories']) !!}
    @include('categories._form', ['submitText' => 'Dodaj kategorijo', 'color' => 'success'])
    {!! Form::close() !!}
    <h2 class="page-header">Vse kategorije</h2>
    @if(count($categories) > 0)
        <table class="table table-bordered table-responsive table-condensed">
            <tr>
                <th>Ime</th>
                <th>Akcija</th>
            </tr>
            @each('categories._category', $categories, 'category')
        </table>
    @else
        <h3 class="text-center">Žal še ni dodanih kategorij</h3>
    @endif
    <div class="modal fade" id="edit-category" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Urejanje kategorije</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name2">Ime kategorije</label>
                        <input type="text" id="name2" name="name" val="" class="form-control">
                        <input type="hidden" id="id" name="id" val="" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="saveCategory()" class="btn btn-primary">Shrani spremembe</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Zapri</button>
                </div>
            </div>
        </div>
    </div>
@endsection