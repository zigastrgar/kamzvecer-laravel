<select class="form-control category_list" multiple="multiple" name="category_list">
    @foreach($categories as $category)
        <option @if($event_categories->contains($category))selected
                @endif value="{{ $category->id }}">{{ $category->name }}</option>
    @endforeach
</select>