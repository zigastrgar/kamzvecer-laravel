<tr data-event="{{ $event->id }}">
    <td>
        <button data-toggle="tooltip" data-placement="bottom" title="Izbriši dogodek" class="btn btn-danger"
                onclick="deleteEvent({{ $event->id }})"><i class="icon icon-android-close"></i>
        </button>
    </td>
    <td contenteditable="true">{{ $event->name }}</td>
    <td contenteditable="true">{{ $event->price }}</td>
    <td>{{ $event->begins_at->format('d. m. Y') }}</td>
    <td>{{ $event->begins_at->format('H:i') }}</td>
    <td contenteditable="true">{{ $event->description }}</td>
    <td>@include('events._venues', ['venues' => $venues])</td>
    <td>@include('events._organizers', ['organizers' => $organizers])</td>
    <td>
        @include('events._categories', ['categories' => $categories, 'event_categories' => $event->categories])
    </td>
    <td>
        <button data-toggle="tooltip" data-placement="bottom" title="Shrani dogodek" class="btn btn-success"
                onclick="saveEvent({{ $event->id }})"><i class="icon icon-floppy-o"></i>
        </button>
        <button data-toggle="tooltip" data-placement="bottom" title="Objavi dogodek" class="btn btn-success"
                data-publish="{{ $event->id }}" onclick="publishEvent({{ $event->id }})"><i
                    class="icon icon-check"></i></button>
        <a data-toggle="tooltip" data-placement="bottom" title="Uredi dogodek"
           href="{{ url("events/$event->id/edit") }}" class="btn btn-primary"><i class="icon icon-pencil"></i></a>
        <a data-toggle="tooltip" data-placement="bottom" title="Odpri povezavo dogodka" class="btn btn-default"
           href="{{ $event->link }}" rel="noreferrer noopener" target="_blank"><i
                    class="icon-link"></i></a>
    </td>
</tr>

@section('scripts')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

        function deleteEvent(id) {
            $.ajax({
                url: '/events/' + id,
                type: 'DELETE',
                data: {'_token': '{{ csrf_token() }}'},
                success: function (response) {
                    if (response === "success") {
                        alertify.success("Dogodek uspešno izbrisan")
                        $("tr[data-event=" + id + "]").remove();
                    } else {
                        alertify.error("Napak pri brisanju dogodka!")
                    }
                }
            })
        }

        function saveEvent(id) {
            var title = $("tr[data-event=" + id + "] td:nth-child(2)").text()
            var price = $("tr[data-event=" + id + "] td:nth-child(3)").text()
            var content = $("tr[data-event=" + id + "] td:nth-child(6)").text()
            var category_list = $("tr[data-event=" + id + "] td:nth-child(9) select").val()
            $.ajax({
                url: '/events/' + id + '/update_description',
                type: 'POST',
                data: {
                    '_token': '{{ csrf_token() }}',
                    'content': content,
                    'title': title,
                    'price': price,
                    'category_list': category_list
                },
                success: function (cb) {
                    if (cb === "success") {
                        alertify.success("Dogodek uspešno spremenjen")
                    } else {
                        alertify.error("Napaka pri urejanju dogodka")
                    }
                }
            })
        }

        function publishEvent(id) {
            $.ajax({
                url: '/events/' + id + '/publish',
                type: 'POST',
                data: {'_token': '{{ csrf_token() }}'},
                success: function (response) {
                    if (response === "success") {
                        alertify.success("Dogodek objavljen!")
                        $("tr[data-event=" + id + "]").remove()
                    } else {
                        alertify.error("Dogodek ni bil uspešno objavljen!")
                    }
                }
            });
        }

        $(document).on("change", "select[name=organizer]", function () {
            var organizer = $(this).val()
            var id = $(this).attr('data-event')
            $.ajax({
                url: '/events/' + id + '/update_organizer',
                type: 'POST',
                data: {'_token': '{{ csrf_token() }}', 'organizer': organizer},
                success: function (cb) {
                    if (cb === "success") {
                        alertify.success("Sprememba uspešna!")
                    } else {
                        alertify.error("Sprememba neuspešna!")
                    }
                }
            })
        });

        $(document).on("change", "select[name=venue]", function () {
            var venue = $(this).val()
            var id = $(this).attr('data-event')
            $.ajax({
                url: '/events/' + id + '/update_venue',
                type: 'POST',
                data: {'_token': '{{ csrf_token() }}', 'venue': venue},
                success: function (cb) {
                    if (cb === "success") {
                        alertify.success("Sprememba uspešna!")
                    } else {
                        alertify.error("Sprememba neuspešna!")
                    }
                }
            })
        });
    </script>

    <script src="{{ asset('/vendor/select2/select2.full.min.js') }}"></script>
    <script>
        $(".category_list").select2({
            palceholder: 'Dodaj zvrsti',
            maximumSelectionLength: 3
        });
    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset("/vendor/select2/select2.min.css") }}">
@endsection