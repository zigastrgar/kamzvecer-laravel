<div class='form-group'>
    {!! Form::label('name', 'Ime dogodka') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('price', 'Cena vstopa') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
    <span class="help-block">Če pustiš prazno pomeni ni podatka</span>
</div>
<div class='form-group'>
    {!! Form::label('description', 'Opis dogodka') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => 6]) !!}
</div>
<div class='form-group'>
    {!! Form::label('begins_at', 'Začetek dogodka') !!}
    {!! Form::text('begins_at', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('ends_at', 'Konec dogodka') !!}
    {!! Form::text('ends_at', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('venue_id', 'Prizorišče') !!}
    {!! Form::select('venue_id', $venues, null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('organizer_id', 'Organizator') !!}
    {!! Form::select('organizer_id', $organizers, null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('category_list', 'Kategorije') !!}
    {!! Form::select('category_list[]', $categories, null, ['class' => 'form-control', 'id' => 'category_list', 'multiple']) !!}
</div>
<div class='form-group'>
    {!! Form::label('published', 'Objavi dogodek') !!}
    {!! Form::checkbox('published', 1, null) !!}
</div>
<div class="form-group">
    {!! Form::submit($submitText, ['class' => "btn btn-$color"]) !!}
</div>

@section('scripts')
    <script src="{{ asset('/vendor/select2/select2.full.min.js') }}"></script>
    <script>
        $("#category_list").select2({
            palceholder: 'Dodaj zvrsti',
            maximumSelectionLength: 3
        });
    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset("/vendor/select2/select2.min.css") }}">
@endsection