<select name="organizer" data-event="{{ $event->id }}"  class="form-control">
    @foreach($organizers as $organizer)
        <option value="{{ $organizer->id }}"
                @if($event->organizer->id == $organizer->id) selected="selected" @endif>
            {{ $organizer->name }}
        </option>
    @endforeach
</select>