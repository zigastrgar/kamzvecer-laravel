<select name="venue" data-event="{{ $event->id }}" class="form-control">
    @foreach($venues as $venue)
        <option value="{{ $venue->id }}"
                @if($event->venue->id == $venue->id) selected="selected" @endif>
            {{ $venue->name }}
        </option>
    @endforeach
</select>