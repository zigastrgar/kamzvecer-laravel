@extends('app')

@section('content')
    <h1 class="page-header">Dodajanje dogodka</h1>
    {!! Form::open(['url' => 'events', 'method' => 'POST']) !!}
    @include('events._form', ['submitText' => 'Dodaj dogodek', 'color' => 'success', 'venues' => $venues, 'organizers' => $organizers])
    {!! Form::close() !!}
@endsection