@extends('app')

@section('content')
    <h1 class="page-header">Urejanje dogodka</h1>
    {!! Form::model($event, ['action' => ['EventsController@update', $event->id], 'method' => 'PATCH']) !!}
    @include('events._form', ['submitText' => 'Uredi dogodek', 'color' => 'primary', 'venues' => $venues, 'organizers' => $organizers])
    {!! Form::close() !!}
@endsection