@extends('app')

@section('content')
    <h1 class="page-header">Dogodki</h1>
    <a href="{{ url('events/create') }}" class="btn btn-success">Dodaj dogodek</a>
    <h2 class="page-header">Vsi dogodki</h2>
    @if(count($events) > 0)
        <table class="table table-bordered table-responsive table-condensed">
            <tr>
                <th>Briši</th>
                <th width="200">Ime</th>
                <th>Vstopnina</th>
                <th width="100">Datum</th>
                <th width="6">Ura</th>
                <th width="500">Opis</th>
                <th width="150">Prizorišče</th>
                <th width="150">Organizator</th>
                <th>Kategorije</th>
                <th width="185">Akcije</th>
            </tr>
            @foreach($events as $event)
                @include('events._event')
            @endforeach
        </table>
    @else
        <h3 class="text-center">Žal še ni dodanih dogodkov</h3>
    @endif
    @if(count($events) > 15)
        <a href="{{ url('events/create') }}" class="btn btn-success">Dodaj dogodek</a>
    @endif
@endsection
<!-- TODO Make a edit modal not new page! -->