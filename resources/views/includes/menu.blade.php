<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Menu</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">KamZvečer automated service</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                @if(Auth::check())
                    <li><a href="{{ url('events') }}">Dogodki</a></li>
                    <li><a href="{{ url('venues') }}">Prizorišča</a></li>
                    <li><a href="{{ url('organizers') }}">Organizatorji</a></li>
                    <li><a href="{{ url('categories') }}">Kategorije</a></li>
                    <li><a href="{{ url('fetch_events') }}">Pridobi dogodke</a></li>
                @else
                    <li><a href="{{ url("login") }}">Prijava</a></li>
                @endif
            </ul>
            @if(Auth::check())
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Živjo, {{ Auth::user()->name }}</a></li>
                </ul>
            @endif
        </div>
    </div>
</nav>