<div class='form-group'>
    {!! Form::label('name', 'Ime') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('address', 'Naslov') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('link', 'Povezava do Facebook strani') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('about', 'O organizatorju') !!}
    {!! Form::textarea('about', null, ['class' => 'form-control', 'rows' => 5]) !!}
</div>
<div class='form-group'>
    {!! Form::label('category_id', 'Privzeta kategorija') !!}
    {!! Form::select('category_id', $categories, null, ['class' => 'form-control']) !!}
</div>
<div class="from-group">
    {!! Form::submit($submitText, ['class' => "btn btn-$color"]) !!}
</div>