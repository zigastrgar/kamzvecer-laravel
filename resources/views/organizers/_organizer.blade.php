<tr data-organizer="{{ $organizer->id }}">
    <td>{{ $organizer->name }}</td>
    <td>{{ $organizer->address }}</td>
    <td>{{ $organizer->about }}</td>
    <td><span class="label label-primary">{{ $organizer->category->name }}</span></td>
    <td>
        <a href="{{ $organizer->link }}" rel="noreferrer noopener" class="btn btn-default" target="_blank">Povezava do strani</a>
        <a href="{{ url("organizers/$organizer->id/edit") }}" class="btn btn-primary">Uredi</a>
        <button class="btn btn-danger" onclick="deleteOrganizer({{ $organizer->id }})">Izbriši</button>
    </td>
</tr>

@section('scripts')
    <script>
        function deleteOrganizer(id) {
            $.ajax({
                url: '/organizers/' + id,
                type: 'DELETE',
                data: {'_token': '{{ csrf_token() }}'},
                success: function (response) {
                    if (response === "success") {
                        $("tr[data-organizer=" + id + "]").remove();
                    }
                }
            })
        }
    </script>
@endsection