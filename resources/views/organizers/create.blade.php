@extends('app')

@section('content')
    <h1 class="page-header">Dodajanje oraganizatorja</h1>
    {!! Form::open(['url' => 'organizers', 'method' => 'POST']) !!}
    @include('organizers._form', ['submitText' => 'Dodaj organizatorja', 'color' => 'success'])
    {!! Form::close() !!}
@endsection