@extends('app')

@section('content')
    <h1 class="page-header">Urejanje oraganizatorja</h1>
    {!! Form::model($organizer, ['action' => ['OrganizersController@update', $organizer->id], 'method' => 'PATCH']) !!}
    @include('organizers._form', ['submitText' => 'Uredi organizatorja', 'color' => 'primary'])
    {!! Form::close() !!}
@endsection