@extends('app')

@section('content')
    <h1 class="page-header">Oraganizatorji</h1>
    <a href="{{ url('organizers/create') }}" class="btn btn-success">Dodaj organizatorja</a>
    <h2 class="page-header">Vse organizatorji</h2>
    @if(count($organizers) > 0)
        <table class="table table-bordered table-responsive table-condensed">
            <tr>
                <th>Ime</th>
                <th>Naslov</th>
                <th>Opis</th>
                <th>Kategorija</th>
                <th>Akcija</th>
            </tr>
            @each('organizers._organizer', $organizers, 'organizer')
        </table>
    @else
        <h3 class="text-center">Žal še ni dodanih organizatorjev</h3>
    @endif
    @if(count($organizers) > 15)
        <a href="{{ url('organizers/create') }}" class="btn btn-success">Dodaj organizatorja</a>
    @endif
@endsection