<div class='form-group'>
    {!! Form::label('name', 'Ime prizorišča') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('address', 'Naslov prizorišča') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('city', 'Mesto prizorišča') !!}
    {!! Form::text('city', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <label for="">Potrjeno prizorišče</label>
    {!! Form::checkbox('approved', 1, null) !!}
    <span class="help-block">Nepotrjena so recimo tista ki jih sistem doda sam ko zazna nekaj novega</span>
</div>
<div class="form-group">
    {!! Form::submit($submitText, ['class' => "btn btn-$color"]) !!}
</div>