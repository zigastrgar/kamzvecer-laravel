<tr data-venue="{{ $venue->id }}">
    <td>{{ $venue->name }}</td>
    <td>{{ $venue->address }}</td>
    <td>{{ $venue->city }}</td>
    <td>
        @if($venue->approved)
            <?php
            $color = 'success';
            $text = 'Potrjen';
            ?>
        @else
            <?php
            $color = 'danger';
            $text = 'Nepotrjen';
            ?>
        @endif
        <span class="label label-{{ $color }}">{{ $text }}</span>
    </td>
    <td>
        @unless($venue->approved)
            <button class="btn btn-success" onclick="approveVenue({{ $venue->id }})">Potrdi</button>
        @endunless
        <a href="{{ $venue->googleMaps }}" rel="noreferrer noopener" class="btn btn-default" target="_blank">Poglej na
            Google Maps</a>
        <a href="{{ url("venues/$venue->id/edit") }}" class="btn btn-primary">Uredi</a>
        <button class="btn btn-danger" onclick="deleteVenue({{ $venue->id }})">Izbriši</button>
    </td>
</tr>

@section('scripts')
    <script>
        function deleteVenue(id) {
            $.ajax({
                url: '/venues/' + id,
                type: 'DELETE',
                data: {'_token': '{{ csrf_token() }}'},
                success: function (response) {
                    if (response === "success") {
                        $("tr[data-venue=" + id + "]").remove();
                    }
                }
            })
        }

        function approveVenue(id) {
            $.ajax({
                url: '/venues/' + id + '/approve',
                type: 'POST',
                data: {'_token': '{{ csrf_token() }}'},
                success: function (response) {
                    if (response === "success") {
                        $("tr[data-venue=" + id + "]>td:nth-child(4)").html("<span class='label label-success'>Potrjen</span>")
                        $("tr[data-venue=" + id + "]>td:nth-child(5)>button:first").remove();
                    }
                }
            })
        }
    </script>
@endsection