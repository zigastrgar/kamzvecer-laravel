@extends('app')

@section('content')
    <h1 class="page-header">Dodajanje prizorišča</h1>
    {!! Form::open(['url' => 'venues', 'method' => 'POST']) !!}
    @include('venues._form', ['submitText' => 'Dodaj prizorišče', 'color' => 'success'])
    {!! Form::close() !!}
@endsection