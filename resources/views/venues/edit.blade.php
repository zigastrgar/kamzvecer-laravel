@extends('app')

@section('content')
    <h1 class="page-header">Urejanje prizorišča</h1>
    {!! Form::model($venue, ['action' => ['VenuesController@update', $venue->id], 'method' => 'PATCH']) !!}
    @include('venues._form', ['submitText' => 'Uredi prizorišče', 'color' => 'primary'])
    {!! Form::close() !!}
@endsection