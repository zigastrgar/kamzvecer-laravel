@extends('app')

@section('content')
    <h1 class="page-header">Prizorišča</h1>
    <a href="{{ url('venues/create') }}" class="btn btn-success">Dodaj prizorišče</a>
    <h2 class="page-header">Vsa prizorišča</h2>
    @if(count($venues) > 0)
        <table class="table table-bordered table-responsive table-condensed">
            <tr>
                <th>Ime</th>
                <th>Naslov</th>
                <th>Mesto</th>
                <th>Status</th>
                <th>Akcije</th>
            </tr>
            @each('venues._venue', $venues, 'venue')
        </table>
    @else
        <h3 class="text-center">Žal še ni dodanih prizorišč</h3>
    @endif
    @if(count($venues) > 15)
        <a href="{{ url('venues/create') }}" class="btn btn-success">Dodaj prizorišče</a>
    @endif
@endsection