<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function() {
    return view('pages.index');
});

Route::get('/login', 'Auth\FacebookController@login')->middleware('guest');
Route::get('/facebook/callback', 'Auth\FacebookController@callback')->middleware('guest');

Route::group([ 'middleware' => 'auth' ], function() {
    // TODO Check for all resource routes
    Route::resource('categories', 'CategoriesController', [ 'except' => [ 'create', 'edit' ] ]);
    Route::resource('events', 'EventsController');
    Route::resource('organizers', 'OrganizersController');
    Route::resource('venues', 'VenuesController');
    Route::get('fetch_events', 'FacebookController@fetchEvents');
    Route::get('import/locals', 'ImportsController@locals');
    Route::get('import/venues', 'ImportsController@venues_generate');
    Route::post('import/event', 'ImportsController@event');
    Route::get('events/{id}/publish', 'EventsController@publish');
    Route::post('events/{id}/publish', 'EventsController@publish');
    Route::post('events/{id}/unpublish', 'EventsController@unpublish');
    Route::post('events/{event}/update_organizer', 'EventsController@update_organizer');
    Route::post('events/{event}/update_venue', 'EventsController@update_venue');
    Route::post('events/{event}/update_description', 'EventsController@update_description');
    Route::post('venues/{id}/approve', 'VenuesController@approve');
    Route::get('export/usernames', 'ExportsController@usernames');
});